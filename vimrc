" Pathogen
execute pathogen#infect()
call pathogen#helptags()

set encoding=utf-8
colorscheme solarized
set background=dark
set t_Co=256
let g:solarized_termcolors = 256
syntax on
filetype plugin indent on
let mapleader = ","
let maplocalleader = "\\"
":h speed-up
set shell=bash\ -f
set scrolloff=3
set lazyredraw
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent
set timeout
set timeoutlen=500
set autoread
set history=1000
set wildmode=list:longest
set wildmenu
set hlsearch
set incsearch
set ignorecase
set number
set wildignore+=.git,*.pyc,*/tmp/*,*.so,*.swp,*.zip
set wildignore+=*.jpg,*.jpeg,*.gif,*.png,*.gif,*.psd,*.o,*.obj,*.min.js
set title
set hidden
set laststatus=2
set showcmd
set showmode
set noswapfile
set backspace=indent,eol,start
set completeopt=menu,longest
set visualbell

" NERDTree Ignore
let NERDTreeIgnore = ['\~$','\.pyc$']

" command-t Escape in terminal
let g:CommandTCancelMap=['<ESC>','<C-c>']

"Fuck this is blazing fast!
let g:CommandTFileScanner='watchman'

" python highlight all
let g:python_highlight_all = 1

" Jedi
let g:jedi#auto_vim_configuration = 0
let g:jedi#popup_select_first = 0
let g:jedi#show_call_signatures = 0
let g:jedi#usages_command = ''

" super tab
let g:SuperTabDefaultCompletionType = "context"

" Ag  
nnoremap <leader>a :Ag<space>

" search highlight off
nnoremap <silent><leader><space> :nohlsearch<CR>

" tab => next buffer , Shift + tab  => previous buffer
nnoremap <silent> <tab> :bn<CR>
nnoremap <silent> <s-tab> :bp<CR>


" keep visual selection after indenting
vmap > >gv
vmap < <gv

" quick way to tidy up a file and get you back to where you was
nnoremap <leader>c gg=G``

" quickly change PWD
nnoremap <leader>cd :cd %:p:h<CR>:pwd<CR>

" For nerd tree toggle quickly
nnoremap <silent><leader>n :NERDTreeToggle <CR>

" Close in background for quick access 
nnoremap <leader>z <C-z>

" rename current file TODO: find a better map
nnoremap <leader>rn :call RenameFile()<cr>

" Windows
" splits
nnoremap <leader>h <C-w>s
nnoremap <leader>v <C-w>v
" moving between windows
nnoremap <c-j> <C-w>j
nnoremap <c-l> <C-w>l
nnoremap <c-k> <C-w>k
nnoremap <c-h> <C-w>h
" resizing windows horizantly
nnoremap + <C-w>+
nnoremap - <C-w>-
" resizing windows vertically
nnoremap > <C-w>>
nnoremap < <C-w><
" only
nnoremap <C-o> <C-w><C-o>

" Quickly edit vim
nnoremap <leader>ev :e $MYVIMRC<cr>
nnoremap <leader>eb :e ~/.vim/bundle/<cr>
nnoremap <leader>es :e ~/.vim/snippets/<cr>
nnoremap <leader>ef :e ~/.vim/ftplugin/<cr>

" " UltiSnips
let g:UltiSnipsJumpForwardTrigger = '<tab>'
let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

" Auto Commands 
augroup bekacho
  autocmd!
  autocmd VimEnter * call WildignoreFromGitignore()
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
  autocmd FocusGained * CommandTFlush
  autocmd BufWritePost * CommandTFlush
  autocmd FileType *
        \ if &omnifunc != '' |
        \   call SuperTabChain(&omnifunc, "<c-p>") |
        \   call SuperTabSetDefaultCompletionType("<c-x><c-u>") |
        \ endif

augroup END

function! RenameFile()
  let old_name = expand('%')
  let new_name = input('New file name: ', old_name, 'file')
  if new_name != '' && new_name != old_name
    exec ':saveas ' . new_name
    cal delete(old_name)
  endif
endfunction

if !exists("g:scm_dirs")
  let g:scm_dirs = [ '.git', '.svn', '.hg' ]
endif

" stolen from derek wyatt !
" https://github.com/derekwyatt/ag.vim/blob/master/autoload/ag.vim#L49
function! FindSCMDir()
  let filedir = expand('%:p:h')
  for candidate in g:scm_dirs
    let dir = finddir(candidate, filedir . ';')
    if dir == candidate
      return '.'
    elseif dir != ""
      let dir = substitute(dir, '/' . candidate, '', '')
      return dir
    endif
  endfor
  return "~"
endfunction

function! WildignoreFromGitignore()
  let gitignore = FindSCMDir() .'/'. '.gitignore'
  if filereadable(gitignore)
    let igstring = ''
    for oline in readfile(gitignore)
      let line = substitute(oline, '\s|\n|\r', '', "g")
      if line =~ '^#' | con | endif
      if line == '' | con  | endif
      if line =~ '^!' | con  | endif
      if line =~ '/$' | let igstring .= "," . line . "*" | con | endif
      let igstring .= "," . line
    endfor
    let execstring = "set wildignore+=".substitute(igstring,'^,','',"g")
    execute execstring
  endif
endfunction
