if $USE_NOSE == 1
    compiler nose
else
    let &makeprg='python manage.py test'
    compiler pyunit
end

" TODO: more and more improvement
if filereadable("manage.py")
    nnoremap <cr> :call MakeGreen()<CR>
    nnoremap <leader>x :w<CR>:!clear;python manage.py test<CR>
else
    nnoremap <leader>x :w<CR>:!clear;python %<CR>
endif
setlocal shiftwidth=4 tabstop=4
